const nodemailer = require('nodemailer');

const transport = nodemailer.createTransport({
  host: process.env.MAILHOG_HOST,
  port: '1025',
  auth: null
});

transport.sendMail({
  from: 'Felipe Araujo de Oliveira <felipearaujooliveirade@gmail.com>',
  to: 'Felipe Araujo de Oliveira <felipearaujooliveirade@gmail.com>',
  subject: 'Testando Mailhog',
  html: '<h1>Funcionou!</h1>'
})